new WOW().init();
/*****************WORK SECTION*********************/
$(document).ready(function () {
  $(".img-content-wrapper").magnificPopup({
    delegate: "a",
    type: "image",
    gallery: {
      enabled: true,
    },
    zoom: {
      enabled: true,
      duration: 300,
      easing: "ease-in-out",
      opener: function (openerElement) {
        return openerElement.is("img")
          ? openerElement
          : openerElement.find("img");
      },
    },
  });
});

/*****************TEAM SECTION*********************/

$(document).ready(function () {
  $(".team-members").owlCarousel({
    autoplay: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      550: {
        items: 2,
      },
      768: {
        items: 3,
      },
    },
    margin: 30,
    items: 3,
    loop: true,
    smartSpeed: 700,
    autoplayHoverPause: true,
    dots: true,
    nav: false,
  });
});

/*************************TESTTIMONIAL SECTION**************************/

$(document).ready(function () {
  $(".testimonial-wrapper").owlCarousel({
    items: 1,
    autoplay: true,
    margin: 30,
    loop: true,
    smartSpeed: 700,
    autoplayHoverPause: true,
    dots: true,
    nav: false,
  });
});

/**********************************CLIENT SECTION************************************ */

$(document).ready(function () {
  $(".client-images").owlCarousel({
    items: 6,
    responsiveClass: true,
    responsive: {
      0: {
        items: 3,
      },
      550: {
        items: 4,
      },
      768: {
        items: 5,
      },
    },
    autoplay: true,
    margin: 30,
    loop: true,
    smartSpeed: 700,
    autoplayHoverPause: true,
    dots: true,
    nav: false,
  });
});
const header = document.querySelector(".header");

const menu = document.querySelector(".header .menu");
const list = document.querySelector(".header .links");

$("document").ready((e) => {
  headerModifier();
});

// HEADER

menu.addEventListener("click", (e) => {
  $(".header .links").slideToggle();
});
window.addEventListener("resize", (e) => {
  headerModifier();
});
const sections = document.querySelectorAll("section");
const links = document.querySelectorAll(".header .link a");

document.addEventListener("scroll", (e) => {
  headerModifier();
});

function headerModifier() {
  console.log(window.innerWidth);
  console.log(list.style.backgroundColor);
  if (window.scrollY > 75) {
    header.style.backgroundColor = "rgba(0,0,0,0.7)";
    if (window.innerWidth < 992) {
      console.log(list.style.backgroundColor);
      list.style.backgroundColor = "rgba(0,0,0,0.7)";
    } else {
      list.style.backgroundColor = "transparent";
    }
  } else {
    header.style.backgroundColor = "transparent";
    list.style.backgroundColor = "transparent";
  }
}
var waypoint = (section) => {
  new Waypoint({
    element: section,
    handler: (direction) => {
      const selector = section.dataset.activeLink;
      links.forEach((link) => {
        link.classList.remove("active");
      });
      $(selector).addClass("active");
    },
    offset: "25%",
  });
  new Waypoint({
    element: section,
    handler: (direction) => {
      const selector = section.dataset.activeLink;
      links.forEach((link) => {
        link.classList.remove("active");
      });
      $(selector).addClass("active");
    },
    offset: "-25%",
  });
};

const createWayPoints = () => {
  sections.forEach((section) => {
    waypoint(section);
  });
};

createWayPoints();
$(".counter").counterUp({
  time: 1500,
});
